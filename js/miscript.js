$(document).ready(function(){
   
    $.fn.scrollInfinite = function(options) {   
        var opciones =  (options);
        var target = opciones.scrollTarget;
        return this.each(function() {
          $.fn.scrollInfinite.init($(this), opciones);
        });      
    };

 $.fn.stopScrollInfinite = function(){
      return this.each(function() {
        $(this).attr('scrollInfinite', 'disabled');
      });
      
  };

$.fn.scrollInfinite.init = function(obj, opciones){
    $(obj).attr('scrollInfinite', 'enabled');     
     $.fn.scrollInfinite.loadContent(obj, opciones);   
 };

var urlContent = null;

 $.fn.scrollInfinite.loadContent = function(obj, opciones){
    var target = opciones.scrollTarget;
    console.log(target);
    var miContenido = $(target).scrollTop() + opciones.heightOffset >= $(document).height() - $(target).height();
    for(page = 1; page <= 5; page++){
        var url = "./" + page + ".html";
         $.ajax({
              type: 'GET',
              url: url,
              data: opciones.contentData,
              success: function(data){
                $(obj).append(data);
                var urlContent = $(data).data("url");
                console.log(urlContent); 
                //console.log(data);            
              },
              dataType: 'html'
         });
   }
  };


//ruta de la pagina 
function ScrolledViewUrl(obj) {
    var docViewTop = $(window).scrollTop();
    var windowHeight = $(window).height()
    var docViewBottom = docViewTop + windowHeight;
    var elemTop = $(obj).offset().top;
    var elemBottom = elemTop + $(obj).height();
    return (((elemBottom <= docViewBottom) || (elemBottom >= docViewBottom)) && (elemTop >= docViewTop));
}
  
$(window).scroll(function (options) {
    var links = $('article');
    //console.log(urlContent);
    for (var i = 0; i < links.length; i++) {
        if (ScrolledViewUrl(links[i])) {
            var href = $(links[i]).attr('data-url');
            location.hash = href;
            break;
        }
    }
});
 });

$(document).ready(function(){
    $('#content').scrollInfinite({
    'contentData': {},
    'scrollTarget': $('#content'), 
    'heightOffset': 10, //10 pixeles antes que termine la página se acaba el desplazamiento
    'beforeLoad': function(){ // antes de la carga muestra el div 
    $('#nomoreresults').css('display', 'block');
    $('#loading').css('display', 'none');                         
    },
  });
});
  
