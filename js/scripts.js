var contador = 1;

//llamada a funcion para integrar articulos
$(document).ready(function () {
    loadScroll();
    $(this).on('scroll', function () {
        if ($(window).scrollTop() + $(window).height() >= $(this).height()) {
            loadScroll();
      }
     contador++;
    });
});

//ruta pagina
$(document).ready(function(){

function ScrollViewUrl(obj) {
    var docViewTop = $(window).scrollTop();
    var windowHeight = $(window).height()
    var docViewBottom = docViewTop + windowHeight;
    var elemTop = $(obj).offset().top;
    var elemBottom = elemTop + $(obj).height();
    return (((elemBottom <= docViewBottom) || (elemBottom >= docViewBottom)) && (elemTop >= docViewTop));
}
$(window).scroll(function (options) {
    var links = $('article');
    for (var i = 0; i < links.length; i++) {
        if (ScrollViewUrl(links[i])) {
            var href = $(links[i]).attr('data-url');
            window.history.pushState('', " ", href);
            break;
        }
    }
});

});

//funcion para integrar articulos
function loadScroll(resp) {
    var i = 1
    while(i < 6) {
        var url = "./" + i + ".html";
        $.get(url, function(resp){
            var content = resp;             
            var valor = $(resp).outerHtml();
            $('#content').append(valor);
            $('article + meta').remove();
            $('article + link').remove();
        })
    i++;       
    }
}

$.fn.each()

//funcion para utiliza que outer regrese valores
$.fn.outerHtml = function(include_scripts) {
    if(include_scripts === undefined){ 
        include_scripts = false; 
    }
    var clone = this.clone();
    var items = $.map(clone, function(element){
         if($.nodeName(element, "script")){
              if(include_scripts){
                     var attributes;
                 if(element.attributes){
                      attributes = $.map(element.attributes, function(attribute){
                      return attribute.name + '="' + attribute.value + '" ';
                       });
                  }
                 return '<' + element.nodeName + ' ' + attributes.join(' ') + ">" + $(element).html() + "</" + element.nodeName +'>';
              }  else {
                     return '';
                    }
         } else {
              return $('<div>').append(element).remove().html();
           }
     });
    return items.join('');
}