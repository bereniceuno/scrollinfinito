//codigo para scroll
$(document).ready(function(){	 
 $.fn.scrollInfinite = function(options) { 	
		var opts = $.extend($.fn.scrollInfinite.defaults, options);  
		var target = opts.scrollTarget;
		if (target == null){
			target = obj; 
	 	}
		opts.scrollTarget = target;
	 
		return this.each(function() {
		  $.fn.scrollInfinite.init($(this), opts);
          //console.log(this);
          //console.log(opts);
		});

  };
  
  $.fn.stopScrollInfinite = function(){
	  return this.each(function() {
	 	$(this).attr('scrollInfinite', 'disabled');
	  });
	  
  };


$.fn.scrollInfinite.loadContent = function(obj, opts){
	 var target = opts.scrollTarget;
     //console.log(target);
	 var miContenido = $(target).scrollTop() + opts.heightOffset >= $(document).height() - $(target).height();
     //console.log(miContenido)
	 for(page = 1; page <= 5; page++){
	 var url = "./" + page + ".html";
	 if (miContenido){
		 if (opts.beforeLoad != null){
			opts.beforeLoad(); 
           // console.log(opts.beforeLoad);
		 }
		 $('#content').children('article').attr('rel', 'loaded');
         console.log($(obj).children('article').attr('rel', 'loaded'));
		 $.ajax({
			  type: 'GET',
			  //url: opts.contentPage,
			  url: url,
			  data: opts.contentData,
			  success: function(data){
				$(obj).append(data); 
				//var objectsRendered = $(obj).children('[rel!=loaded]');
				
				//if (opts.afterLoad != null){
				//	opts.afterLoad(objectsRendered);	
				//}
			  },
			  dataType: 'html'
		 });

	 }
	 };
  };
/*var count = 0;
$.fn.scrollInfinite.loadContent = function(obj, opts) {
    var target = opts.scrollTarget;
    var miContenido = $(target).scrollTop() + opts.heightOffset >= $(document).height() - $(target).height();
    if (miContenido) {
        if (opts.beforeLoad != null) {
            opts.beforeLoad();
        }
    $(obj).children().attr('rel', 'loaded');
    $.ajax({
        type: 'GET',
        url: opts.contentPage,
        data: "{'currentCount':'" + count + "'}",
        contentType: "application/json; charset=utf-8",
        success: function(data) {
            $(obj).append(data.d);
            var objectsRendered = $(obj).children('[rel!=loaded]');
            count++;
            if (opts.afterLoad != null) {
                opts.afterLoad(objectsRendered);
            }
        },
        error:
            function(XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
        dataType: "json"

  });
 }
};*/
  

 
  $.fn.scrollInfinite.init = function(obj, opts){
	 var target = opts.scrollTarget;
	 $(obj).attr('scrollInfinite', 'enabled');
	
	 $(target).scroll(function(event){
		if ($(obj).attr('scrollInfinite') == 'enabled'){
	 		$.fn.scrollInfinite.loadContent(obj, opts);	
            console.log($.fn.scrollInfinite.loadContent(obj, opts));
		}
		else {
			event.stopPropagation();	
		}
	 });
	 
	 $.fn.scrollInfinite.loadContent(obj, opts);
	 
 };
	
 $.fn.scrollInfinite.defaults = {
      	// 'contentPage' : null,
     	 'contentData' : {},
		 'beforeLoad': null,
		 'afterLoad': null,
		 'scrollTarget': null,
		 'heightOffset': 0,
 };	

 //codigo para cambio de url
function ScrolledViewUrl(elem) {
	//toma las dimensiones de la ventana
    var docViewTop = $(window).scrollTop();
    var windowHeight = $(window).height()
    var docViewBottom = docViewTop + windowHeight;
    //toma las dimensiones del elemento respecto del documento
    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    

   // console.log(docViewBottom, windowHeight, elemBottom, elemTop, docViewTop);
    //regresa verdadero siempre y cuando el largo del elemento sea menor o igual al largo del documento
    //regresa verdadero siempre y cuando la posicion del  elemento sea mayor al inicio del la pagina
    return (((elemBottom <= docViewBottom) || (elemBottom >= docViewBottom)) && (elemTop >= docViewTop));

}

$(window).scroll(function (e) {
    var links = $('article');
    for (var i = 0; i < links.length; i++) {
        if (ScrolledViewUrl(links[i])) {
            var href = $(links[i]).attr('data-url');
            //location.hash = href.slice(href.indexOf('#') + 1);
            location.hash = href.slice();
            //location = href.slice(href.indexOf('#') + 1);
            break;
        }
    }
});

$(function () {
    var hash = location.hash;
    //var hash = location;
    if (hash) {
        var links = $('article');
        for (var i = 0; i < links.length; i++) {
            if ($(links[i]).attr('data-url') === hash) {
                $(window).scrollTop($(links[i]).offset().top);
                break;
            }
        }
    }
});


});

